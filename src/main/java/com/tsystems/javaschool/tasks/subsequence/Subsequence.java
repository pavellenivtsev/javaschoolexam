package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null) {
            throw new IllegalArgumentException();
        }
        if (y == null) {
            throw new IllegalArgumentException();
        }
        if (x.isEmpty()) return true;
        first:
        for (int j = 0; j < y.size(); j++) {
            if (x.isEmpty()) return true;
            if (x.get(0).equals(y.get(j))) {
                x.remove(0);
                continue first;
            }
        }
        return false;
    }
}