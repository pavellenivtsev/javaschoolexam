package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {


    private boolean incorrect(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) return true;
        if (inputNumbers.size()>50) return true;
        int a = 3;
        int b = 2;
        boolean t = true;
        for (int i = 0; i < 10; i++) {
            if (inputNumbers.size() != a) t = false;
            b++;
            a = a + b;
        }
        return t;
    }

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int a = 3;
        int b = 2;
        int c = 3;
        while (inputNumbers.size() != a) {
            b++;
            a = a + b;
            c += 2;
            if (a == 55) {
                break;
            }
        }
        int[][] twoDimArray;
        if (incorrect(inputNumbers)) throw new CannotBuildPyramidException();
        else {
            Collections.sort(inputNumbers);
            while (inputNumbers.size() != a) {
                b++;
                a = a + b;
                c += 2;
                if (a == 55) {
                    break;
                }
            }
            twoDimArray = new int[b][c];
            for (int i = 0; i < twoDimArray.length; i++) {
                for (int j = 0; j < twoDimArray[0].length; j++) {
                    twoDimArray[i][j] = 0;
                }
            }
            Iterator<Integer> iterator = inputNumbers.iterator();
            b -= 1;
            int d = b;
            for (int i = 0; i < twoDimArray.length; i++) {
                for (int n = b; n <= d; ) {
                    if (iterator.hasNext()) twoDimArray[i][n] = iterator.next();
                    n += 2;
                }
                b -= 1;
                d += 1;
            }
        }
        return twoDimArray;
    }
}
