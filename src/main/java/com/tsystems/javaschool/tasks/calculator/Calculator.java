package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (!check(statement)) return null;
        String result;
        try {
            Pattern pattern = Pattern.compile("(\\d+\\.?\\d?+)+|(\\+|\\-|\\*|/)|(\\(+|\\))");
            Matcher m = pattern.matcher(statement);
            ArrayList<String> list = new ArrayList<>();
            while (m.find()) {
                list.add(m.group(0));
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals(")")) {
                    for (int j = i; j > -1; j--) {
                        if (list.get(j).equals("(")) {
                            ArrayList<String> list1 = new ArrayList<>();
                            for (int k = j + 1; k < i; k++) {
                                list1.add(list.get(k));
                            }
                            for (int h = i; h > j - 1; h--) {
                                list.remove(h);
                            }
                            list.add(j, withoutBraces(list1));
                            break;
                        }
                    }
                }
            }
            result = withoutBraces(list);
        } catch (Exception e) {
            return null;
        }
        return rounding(result);
    }

    public String withoutBraces(ArrayList<String> list) {
        double res;
        for (int l = 0; l < list.size(); l++) {
            if (list.get(l).equals("*") || list.get(l).equals("/")) {
                if (list.get(l).equals("*")) {
                    res = Double.parseDouble(list.get(l - 1)) * Double.parseDouble(list.get(l + 1));
                    list.set(l, Double.toString(res));
                    list.remove(l - 1);
                    list.remove(l);
                    l--;
                } else {
                    res = Double.parseDouble(list.get(l - 1)) / Double.parseDouble(list.get(l + 1));
                    list.set(l, Double.toString(res));
                    list.remove(l - 1);
                    list.remove(l);
                    l--;
                }
            }
        }
        for (int n = 0; n < list.size(); n++) {
            if (list.get(n).equals("+") || list.get(n).equals("-")) {
                if (list.get(n).equals("+")) {
                    if (n >= 2 && list.get(n - 2).equals("-")) {
                        res = Double.parseDouble(list.get(n - 1)) - Double.parseDouble(list.get(n + 1));
                        list.set(n, Double.toString(res));
                        list.remove(n - 1);
                        list.remove(n);
                        n--;
                    } else {
                        res = Double.parseDouble(list.get(n - 1)) + Double.parseDouble(list.get(n + 1));
                        list.set(n, Double.toString(res));
                        list.remove(n - 1);
                        list.remove(n);
                        n--;
                    }
                }
                if (list.get(n).equals("-")) {
                    if (n >= 2 && list.get(n - 2).equals("-")) {
                        res = Double.parseDouble(list.get(n - 1)) + Double.parseDouble(list.get(n + 1));
                        list.set(n, Double.toString(res));
                        list.remove(n - 1);
                        list.remove(n);
                        n--;
                    } else {
                        res = Double.parseDouble(list.get(n - 1)) - Double.parseDouble(list.get(n + 1));
                        list.set(n, Double.toString(res));
                        list.remove(n - 1);
                        list.remove(n);
                        n--;
                    }
                }
            }
        }
        return list.get(0);
    }

    public boolean check(String statement) {
      if (statement==null) return false;
      int a = 0;
        int b = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == ')') a++;
            if (statement.charAt(i) == '(') b++;
        }
        if (a != b) return false;
        if (statement.contains(",")) return false;
        char c1 = '.';
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '.') {
                if (statement.charAt(i) == c1) return false;
            }
            c1 = statement.charAt(i);
        }
        return true;
    }

    public static String rounding(String result) {
        if (result.equals("Infinity")) return null;
        if (!result.contains(".")) return result;
        DecimalFormat formatter = new DecimalFormat("#.####");
        String res = formatter.format(Double.parseDouble(result));
        res = res.replace(",", ".");
        return res;
    }
}
